package com.example.demo;

import com.example.demo.dao.SubjectDao;
import com.example.demo.entity.Subject;
import javafx.application.Application;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
//@EnableTransactionManagement
//@EntityScan("com.example.demo.entity")
//@ComponentScan()
//@Configuration()
//@ComponentScan({"com.example.demo.services","com.example.demo.restcontroller"})
//@EnableJpaRepositories("com.example.demo.repositories")
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
		/*ApplicationContext applicationContext =SpringApplication.run(DemoApplication.class, args);
		SubjectDao subjectDao = applicationContext.getBean(SubjectDao.class);

		int status = subjectDao.saveSubject(new Subject(15, "data structure", "DS", "ECE"));
System.out.println(status);*/
    }
}