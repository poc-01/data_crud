package com.example.demo.controller;

import com.example.demo.entity.Department;
import com.example.demo.model.Request;
import com.example.demo.model.Response;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.StudentService;
import com.example.demo.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "department_data")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;
        @PostMapping(value="department")
//    @RequestMapping(value = "student", method= RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Response<Department> createDeparment(@RequestBody Request<Department> request) {
        Department  department= request.getData();
        department= departmentService.save(department);
        Response response = new Response();
        response.setData(department);
        response.setStatus("SUCCESS");
        return response;
    }
}