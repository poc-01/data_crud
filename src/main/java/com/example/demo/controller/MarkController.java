package com.example.demo.controller;

import com.example.demo.entity.Marks;
import com.example.demo.service.MarkService;
import com.example.model.Request;
import com.example.model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "marks_data")
public class MarkController {

    @Autowired
    private MarkService markService;

    @PostMapping(value = "marks")
    public Response<Marks> createMark(@RequestBody Request<Marks> request) {
        Marks marks = request.getData();
        marks = markService.save(marks);
        Response response = new Response();
        response.setData(marks);
        response.setStatus("Success");
        return response;
    }
}
