package com.example.demo.controller;

import com.example.demo.model.Request;
import com.example.demo.model.Response;
import com.example.demo.service.StudentService;
import com.example.demo.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "student_data")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PostMapping(value = "student")
//    @RequestMapping(value = "student", method= RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Response<Student> createStudent(@RequestBody Request<Student> request) {
        Student student = request.getData();
        student = studentService.save(student);
        Response response = new Response();
        response.setData(student);
        response.setStatus("SUCCESS");
        return response;
    }

    @GetMapping("student/{id}")
    public Response<Student> getStudent(@PathVariable(required = true) int id) {
        Student student = studentService.getStudentById(id);
        Response response = new Response();
        response.setData(student);
        response.setStatus("SUCCESS");
        return response;
    }
}
