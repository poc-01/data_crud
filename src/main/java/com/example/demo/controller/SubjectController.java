package com.example.demo.controller;


import com.example.demo.entity.Subject;
import com.example.demo.model.Request;
import com.example.demo.model.Response;
import com.example.demo.service.SubjectService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "subject_data")
public class SubjectController {

    @Autowired
    private SubjectService subjectService;

    @PostMapping(value = "subject")
//    @RequestMapping(value = "student", method= RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Response<Subject> createSubject(@RequestBody Request<Subject> request) {
        Subject subject = request.getData();
        subject = subjectService.save(subject);
        Response response = new Response();
        response.setData(subject);
        response.setStatus("SUCCESS");
        return response;
    }


}
