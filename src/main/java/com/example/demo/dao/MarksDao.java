package com.example.demo.dao;

import com.example.demo.entity.Marks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class MarksDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Marks save(Marks m) {
        int count = 0;
        try {
            count = jdbcTemplate.update("INSERT INTO SP_TEST.MARKS (SEM,MARKS) VALUES (?,?)",
                    new Object[]{m.getSem(), m.getMarks()});
            System.out.println("Record inserted count " + count);
            return m;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
