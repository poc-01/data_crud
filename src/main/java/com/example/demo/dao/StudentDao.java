package com.example.demo.dao;

import com.example.demo.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class StudentDao {
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Student getStudentById(int id) {
        String sql = "SELECT * FROM STUDENT WHERE ID=:ID";
//        String sql = "SELECT * FROM STUDENT WHERE ID="+id;
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("ID", id);
//                .addValue("NAME", String.class);
        Student student = (Student) namedParameterJdbcTemplate.queryForObject(sql, sqlParameterSource, new BeanPropertyRowMapper(Student.class));
        return student;

    }
}
