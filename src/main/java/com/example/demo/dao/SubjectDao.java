package com.example.demo.dao;

import com.example.demo.entity.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SubjectDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Subject save(Subject s) {
        int count = 0;
        try {
            System.out.println("save started");
            count = jdbcTemplate.update("INSERT INTO SUBJECT (ID,NAME,CODE,DEPARTMENT_CODE) VALUES (?,?,?,?)",
                    new Object[]{s.getId(), s.getName(), s.getCode(), s.getDepartmentCode()});
            System.out.println("Records inserted count " + count);
            return s;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("save completed");
        }
        return null;
    }

}
