package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "SP_TEST", name = "MARKS")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Marks {
    @Id
    @Column(name = "SEM")
    private int sem;
    @Column(name = "MARKS")
    private Float marks;

}
