package com.example.demo.repositories;

import com.example.demo.entity.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
public interface SubjectRepository  extends JpaRepository<Subject, Integer> {
}
