package com.example.demo.service;

import com.example.demo.entity.Department;
import com.example.demo.repositories.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    public Department save(Department department) {
        Department department1 = departmentRepository.save(department);
        return department1;
    }
}
