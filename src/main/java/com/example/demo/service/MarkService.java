package com.example.demo.service;

import com.example.demo.dao.MarksDao;
import com.example.demo.entity.Marks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MarkService {
    @Autowired
    MarksDao marksDao;

    public Marks save(Marks marks) {
        Marks marks1 = marksDao.save(marks);
        return marks1;
    }

}
