package com.example.demo.service;

import com.example.demo.dao.StudentDao;
import com.example.demo.dao.SubjectDao;
import com.example.demo.repositories.StudentRepository;
import com.example.demo.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentDao studentDao;


    public Student save(Student student) {
        Student student1 = studentRepository.save(student);
        return student1;
    }

    public Student getStudentById(int id){
        Student student = studentDao.getStudentById(id);
        return student;
    }
}
