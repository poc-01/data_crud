package com.example.demo.service;

//import com.example.demo.dao.StudentDao;
import com.example.demo.dao.SubjectDao;
import com.example.demo.entity.Student;
import com.example.demo.repositories.SubjectRepository;
import com.example.demo.entity.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class SubjectService {

   @Autowired
    private SubjectDao subjectDao;


     public Subject save(Subject subject){
        Subject subject1 = subjectDao.save(subject);
        return subject1;
    }

}
